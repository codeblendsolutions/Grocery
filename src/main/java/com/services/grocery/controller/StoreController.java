package com.services.grocery.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/grocery")
public class StoreController {


    @GetMapping("/storeList")
    public Mono<String> getStoreList(){
        return Mono.just("we will get that");
    }
}
